# open-learner-model

## What is it ?

Open Learner Model is a visualisation tools bundle.

### Dependencies

The olm bundle contains [d3 v6.2.0](https://d3js.org/). 
This project uses [bootstrap](https://getbootstrap.com/) and [OverlayScrollbars](https://github.com/KingSora/OverlayScrollbars) for its demos.
Thanks to [@stopyransky](https://codepen.io/stopyransky/pen/EXdrOo) for the hierarchy chart codepen.


## How to use ?

Just include the file `olm.bundle.js` from the `public` folder and that's it. Now the OLM library is available as `document._OLM`.

```HTML
<!DOCTYPE html>
<html>
    <head>
        <script type="text/javascript" src="./olm.bundle.js"></script>
        <title>OLM example</title>
    </head>
    <body>

    </body>
    <script>
        (function() {
            var OLM = document._OLM;
            // and so on...
        })();
    </script>
</html>
```

## The charts

### Tree Indented

Follow the [TreeIndented demo link](https://lachand.gitlab.io/open-learner-model/treeindented.html).

The config & its default values :
```JSON
{
    "fontHoverColor": 'rgba(255, 255, 255, 1)',
    "fontColor": 'rgba(255, 255, 255, .85)',
    "colors": {
            "to": 0.25,
            "color": "#cf000f"
        },
        {
            "to": 0.5,
            "color": "#f57f17"
        },
        {
            "to": 0.75,
            "color": "#ffee58"
        },
        {
            "color": "#4caf50"
        }
    ],
    "showMastery": true,
    "showTrust": true,
    "showCover": true,
    "formatMastery": 'percentage',    /* <== '2decimal' & 'percentage' available. */
    "formatTrust": 'percentage',    /* <== No more option so far...             */
    "formatCover": 'percentage'     /* <== No more option so far...             */
}
```  

How to use :
```javascript
var OLM = document._OLM;
// You can create a random framework to try it out. 
// let framework = OLM.CORE.Utils.getScoredFrameworkSample();

// Creates a tree based on the framework.
let fw_tree = new OLM.CORE.FrameworkTree();
fw_tree.buildFromFramework(framework);

// Creates the TreeIndented object.
let treeIndented  = new OLM.TreeIndented(document.getElementById('test'), fw_tree, config);
TreeIndented.onMouseOver = (node) => {
  // Your mouseOver behavior here.
}
// We chose an id for the svg element. Default behavior automatically creates a unique id.
TreeIndented.draw(svgId = 'test-indented');
```




### Tree Pack / Partition / Sunburst

Follo the [TreePack demo link](https://lachand.gitlab.io/open-learner-model/treepack.html),
the [TreePartition demo link](https://lachand.gitlab.io/open-learner-model/treepartition.html),
and the [TreeSunburst demo link](https://lachand.gitlab.io/open-learner-model/treesunburst.html).

The config & its default values :
```JSON
{
    "fontColor": 'rgba(255, 255, 255, .85)',
    "backgroundColor": '#343a40',                // Only changes the help background color.
    "formatMastery": 'percentage',
    "formatTrust": 'percentage',
    "formatCover": 'percentage',
    "useHash": true, 
    "hashTreshold": 0.1,
    "useLegend": true,
    "colors": {
            "to": 0.25,
            "color": "#cf000f"
        },
        {
            "to": 0.5,
            "color": "#f57f17"
        },
        {
            "to": 0.75,
            "color": "#ffee58"
        },
        {
            "color": "#4caf50"
        }
    ],
    "noValueColor": "#808080"
}
```  

How to use :
```javascript
var OLM = document._OLM;
// You can create a random framework to try it out. 
// let framework = OLM.CORE.Utils.getScoredFrameworkSample();

// Creates a tree based on the framework.
let fw_tree = new OLM.CORE.FrameworkTree();
fw_tree.buildFromFramework(framework);

// Creates the TreePartition object.
let treePack  = new OLM.TreePack(document.getElementById('test'), fw_tree, config);
treePartition.onMouseOver = (node) => {
  // Your mouseOver behavior here.
}
// We chose an id for the svg element. Default behavior automatically creates a unique id.
treePack.draw(svgId = 'test-pack');
```
